package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String,Integer> barcodeAmount = getBarcodeAndAmount(barcodes);
        List<ReceiptItem> receiptItems = decodeToItems(barcodeAmount);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }
    public List<Item> loadAllItems(){
        return ItemsLoader.loadAllItems();
    }
    public Map<String,Integer> getBarcodeAndAmount(List<String> barcodes){
        Map<String,Integer> barcodesAmount = new HashMap<>();
        for( String barcode : barcodes){
            if(barcodesAmount.containsKey(barcode)){
                int amount = barcodesAmount.get(barcode);
                barcodesAmount.put(barcode,amount+1);
            }else{
                barcodesAmount.put(barcode,1);
            }
        }
        return barcodesAmount;
    }

    public  List<ReceiptItem> decodeToItems(Map<String,Integer> barcodesAmount){
        List<Item> itemList = loadAllItems();
        List<ReceiptItem> receiptItems = new ArrayList<>();
        Set<String> barcodeSet = barcodesAmount.keySet();
        List<String> barcodes = new ArrayList<>();
        for(String barcode : barcodeSet){
            barcodes.add(barcode);
        }
        Collections.sort(barcodes);
        for(String barcode : barcodes){
            for(Item item : itemList){
                if(item.getBarcode().equals(barcode)){
                    ReceiptItem receiptItem = new ReceiptItem(item.getName(), item.getBarcode(), barcodesAmount.get(item.getBarcode()), item.getPrice());
                    receiptItems.add(receiptItem);
                }
            }
        }
        return receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems){
        int totalPrice = 0;
        for(ReceiptItem receiptItem : receiptItems){
            totalPrice += receiptItem.getTotalPrice();
        }
        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems){
        int price = calculateTotalPrice(receiptItems);
        return new Receipt(receiptItems,price);
    }

    public String generateItemReceipt(Receipt receipt){
        String text = "";
        List<ReceiptItem> receiptItems = receipt.getReceiptItemList();
        for(ReceiptItem receiptItem : receiptItems){
            String item = "Name: " + receiptItem.getName() + ", Quantity: " +
                    receiptItem.getAmount() + ", Unit price: " + receiptItem.getPrice() +
                    " (yuan), Subtotal: " + receiptItem.getTotalPrice() + " (yuan)\n";
            text += item;
        }
        return text;
    }

    public String generateReceipt(String itemsReceipt, int totalPrice){
        String line = "----------------------\n";
        String price = "Total: " + totalPrice + " (yuan)\n";
        return itemsReceipt + line + price;
    }

    public String renderReceipt(Receipt receipt){
        String header = "***<store earning no money>Receipt***\n";
        String footer = "**********************";
        String itemsReceipt = generateItemReceipt(receipt);
        String receiptBody = generateReceipt(itemsReceipt,receipt.getTotalPrice());
        return header + receiptBody + footer;

    }
}
