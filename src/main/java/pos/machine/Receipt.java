package pos.machine;

import java.util.List;

public class Receipt {
    List<ReceiptItem> receiptItemList;
    int totalPrice;

    public Receipt(List<ReceiptItem> receiptItemList, int totalPrice) {
        this.receiptItemList = receiptItemList;
        this.totalPrice = totalPrice;
    }

    public List<ReceiptItem> getReceiptItemList() {
        return receiptItemList;
    }

    public void setReceiptItemList(List<ReceiptItem> receiptItemList) {
        this.receiptItemList = receiptItemList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
