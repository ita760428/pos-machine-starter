package pos.machine;

public class ReceiptItem {
    private String name;
    private  String barcode;
    private int amount;

    private int price;

    private int totalPrice;


    public ReceiptItem(String name, String barcode, int amount, int price) {
        this.name = name;
        this.barcode = barcode;
        this.amount = amount;
        this.price = price;
        this.totalPrice = price * amount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
